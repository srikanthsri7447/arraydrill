const map = function(elements,cb){
    if(!elements || !cb){
        return []
    }
    else{
        let newArray = []
        for (let i=0; i<elements.length; i++){

            newArray.push(cb(elements[i]));
        
        }
        return newArray;
    }
    
    
}

module.exports = map;