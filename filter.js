function filter(elements, cb){

    if(!elements || !cb){
        return []
    }
    else{

        let primeArray = []

        for(let i=0; i<elements.length; i++){
            
            const primNum = cb(elements[i])

            if(primNum){
                primeArray.push(primNum)
            }
            
            
        }
        return primeArray
        
    }
}

module.exports = filter;