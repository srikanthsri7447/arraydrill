function each(elements, cb){

    if(!elements || !cb ){
        return []
    }
    else{
        let output = []

        for(let i=0; i<elements.length; i++){

            output.push(cb(elements[i]));
        }
        return output;
    }

}

module.exports = each;