function find(elements, cb){

    if(!elements || !cb ){
        return []
    }
    else{

        let output = []

        for(let i=0; i<elements.length; i++){
            const isTrue = cb(elements[i])
            if(isTrue){
                output.push(elements[i])
            }
        }
        return output;
        
    }

}


module.exports = find;