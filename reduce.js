function reduce(elements, cb, startingValue){

    if(!elements || !cb){

        return []

    }
    else{

        if(!startingValue){
           

            let startingValue = elements[0]

            for(let i=0; i<elements.length; i++){

                startingValue = cb(startingValue,elements[i])

            }
            return startingValue
        }
        else{

            for(let i=0; i<elements.length; i++){

                startingValue = cb(startingValue,elements[i])
                
              
            }
            return startingValue
        }
    }

    

}

module.exports = reduce;