const reduce = require("../reduce")

const sumOfArray = function(acc,curr){
    return acc + curr
}

const items = [1, 2, 3, 4, 5, 5]; 

const result = reduce(items,sumOfArray,1)
console.log(result)