const flatten = require("../flatten")

const nestedArray = [1,[2.45,[true]],[[[[false]]]]];

const result = flatten(nestedArray);
console.log(result);