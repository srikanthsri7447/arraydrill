const filter = require("../filter")

const isPrime = function (num){
    if(num>=2){

        let count =0

        for(let i=1; i<(num+1); i++){
            if(num%i===0){
                count++
            }
        }
        if(count==2){
            return num
        }
        else{
            return ''
        }
    }
    else{
        return ''
    }
   
}

const items = [1, 2, 3, 4, 5, 6, 7, 11];

const result = filter(items,isPrime);

console.log(result)